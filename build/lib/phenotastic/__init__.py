# -*- coding: utf-8 -*-

"""Top-level package for Phenotastic."""
import os
import sys
import warnings

import numpy as np
import vtk

__author__ = """Henrik Ahl"""
__email__ = "hpa22@cam.ac.uk"
__version__ = "0.3.0"
