phenotastic package
===================

Submodules
----------

phenotastic.domains module
--------------------------

.. automodule:: phenotastic.domains
   :members:
   :undoc-members:
   :show-inheritance:

phenotastic.mesh module
-----------------------

.. automodule:: phenotastic.mesh
   :members:
   :undoc-members:
   :show-inheritance:

phenotastic.misc module
-----------------------

.. automodule:: phenotastic.misc
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: phenotastic
   :members:
   :undoc-members:
   :show-inheritance:
